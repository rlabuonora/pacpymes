Rails.application.routes.draw do
  



  resources :users do
    resources :firms
  end
  
  root 'static_pages#home'
  get     'contact' => 'static_pages#contact'
  get     'about'   => 'static_pages#about'
  get     'how'     => 'static_pages#how'
  get     'signup'  => 'users#new'
  get     'login'   => 'static_pages#home'
  post    'login'   => 'sessions#create'
  delete  'logout'  => 'sessions#destroy'
end
