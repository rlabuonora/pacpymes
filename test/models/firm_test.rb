require 'test_helper'

class FirmTest < ActiveSupport::TestCase
  def setup
    @user = users(:archer)
    @asesor = users(:rafa)
    @firm = @user.firms.new(name: "Archer's Firm",
                                    legal: "Archer Inc.",
                                    industry: "Commerce",
                                    sales: 1000000,
                                    employees: 90)
  end
  
  
  test "should be valid" do
    assert @firm.valid?
  end
  
  test "name should be present" do
    @firm.name = ""
    assert_not @firm.valid?
  end
  
  test "legal should be present" do
    @firm.legal = ""
    assert_not @firm.valid?
  end
  
  test "sales should be present" do
    @firm.sales = ""
    assert_not @firm.valid?
  end
  
  test "employees should be present" do
    @firm.employees = ""
    assert_not @firm.valid?
  end
  
  test "name cannot be duplicated" do
    @firm.save
    new_firm = Firm.new(name: "New Firm", legal: "Legal", industry: "Fishing", sales: 90000, employees: 92)
    assert new_firm.valid?
    new_firm.name = @firm.name
    assert_not new_firm.valid?
  end
  
  test "legal cannot be duplicated" do
    @firm.save
    new_firm = Firm.new(name: "New Firm", legal: "Legal", industry: "Fishing", sales: 90000, employees: 92)
    assert new_firm.valid?
    new_firm.legal = @firm.legal
    assert_not new_firm.valid?
  end
  
  test "check yml relations are ok" do
    firm = firms(:one)
    archer = users(:archer)
    assert_equal firm.user, archer
  end
  
  test "asesor must be admin" do
    firm = firms(:one)
    assert firm.valid?
    firm.asesor = @user
    assert_not firm.valid?
  end
end
