require 'test_helper'

class UserTest < ActiveSupport::TestCase
 
 def setup
  @user = User.new(name: "Rafael La Buonora", email: "rlabuonora@gmail.com",
                   password: "password", password_confirmation: "password")
 end
 
 test "should be valid" do
   assert @user.valid?
 end
 
 test "name should be present" do
   @user.name = "  "
   assert_not @user.valid?
 end
 
 test "email should be present" do
   @user.email = "  "
   assert_not @user.valid?
 end
 
 test "name should not be too long" do
   @user.name = "a" * 51
   assert_not @user.valid?
  end
  
  
  test "email should not be too long" do
   @user.email = "a" * 255 + "@example.com"
   assert_not @user.valid?
  end
  
   test "email validations should accept valid addresses" do
    valid_emails = %w(user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn)
    
    valid_emails.each do |email|
      @user.email = email
      assert @user.valid?, "#{email.inspect} should be valid"
    end
  end
  
  test "email validations should not accept invalid adresses" do
    invalid_emails = %w(user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com)
    invalid_emails.each do |email|
      @user.email = email
      assert_not @user.valid?, "#{email.inspect} should not be valid"
    end
  end
  
  test "email cannot be duplicated" do
    duplicated_user = @user.dup
    @user.save
    assert_not duplicated_user.valid?
  end
  
  test "email cannot be duplicated without case sensitivity" do
    duplicated_user = @user.dup
    duplicated_user.email = @user.email.upcase
    @user.save
    assert_not duplicated_user.valid?
  end
  
  test "user with non matching password confirmation is not valid" do
    @user.password_confirmation = "foobaz"
    assert_not @user.valid?
  end
  
  test "password must be 6 chars" do
    @user.password = @user.password_confirmation = "x" * 5
    assert_not @user.valid?
  end
  
  test "Every user returned by admin scope is admin" do
    admins = User.admins
    admins.each do |admin|
      assert admin.admin?  
    end
  end
  
  test "admin scope returns all admins" do
    admins = User.admins
    User.all.each do |user|
      assert admins.include?(user) if user.admin?
    end
  end
  
 
end
