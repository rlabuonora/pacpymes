require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  
  test "user signup with invalid data" do
    get signup_path
    assert_template 'users/new'
    assert_no_difference "User.count" do
      post users_path, user: { name:                  " ",
                               email:                 "invalid@invalid",
                               password:              "password",
                               password_confirmation: "password" }
      
    end
    assert_template 'users/new'
    assert_select "div.field_with_errors"
    assert_select "div#error_explanation"
    
  end
  
  test "user signup with valid data" do
    get signup_path 
    assert_template 'users/new'
    assert_difference "User.count", 1 do
      post_via_redirect users_path, user: { name:                  "Example User",
                                            email:                 "user@example.org",
                                            password:              "password",
                                            password_confirmation: "password" }
    end
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
    
  end
end
