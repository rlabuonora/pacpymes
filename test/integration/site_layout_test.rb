require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  
  test "site layout" do
    get root_path
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", how_path
  end
  
  test "if user is logged in redirects to home" do
    user = users(:rafa)
    log_in_as user
    get root_path
    assert_redirected_to user_path user
  end
end
