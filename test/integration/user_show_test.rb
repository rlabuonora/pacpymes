require 'test_helper'

class UserShowTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:archer)
    @admin = users(:rafa)
  end
  
  test "showing a non admin user profile shows its firms" do
    log_in_as @user
    other_user = users(:lana)
    assert_equal 1,  other_user.firms.count
    get user_path other_user
    other_user.firms.each do |firm|
      assert_match firm.name, response.body
    end
    
    assert_response :success
  end
  
  test "showing an admin user profile shows firms it asesora" do
    log_in_as @admin
    get user_path @admin
    @admin.asesoradas.each do |firm|
      assert_match firm.name, response.body
    end
  end
end
