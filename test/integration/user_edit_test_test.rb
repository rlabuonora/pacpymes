require 'test_helper'

class UserEditTestTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:rafa)
    @non_admin_user = users(:lana)
  end
  
  test "edit user with invalid data" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:                  "",
                                    email:                 "rlabuonota@gmial.com",
                                    password:              "foo",
                                    password_confirmation: "bar" }
    assert_template 'users/edit'
    assert_select "div.field_with_errors"
    assert_select "div#error_explanation"
    
  end
  
  
  test "edit user with valid data and friendly forwarding" do
    get edit_user_path @user
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    follow_redirect!
    assert_template 'users/edit'
    new_email = "rlabuonora@gmail.com"
    new_name = "Rafa La Buonora"
    patch user_path(@user), user: { name:                  new_name,
                                    email:                 new_email,
                                    password:              "",
                                    password_confirmation: "" 
                                  }
    assert_redirected_to @user
    @user.reload
    assert_equal @user.email, new_email
    assert_equal @user.name, new_name
    assert_not flash.empty?
  end
  
  test "admin users as admin, edit and delete" do
    log_in_as(@user)
    get users_path
    User.paginate(page: 1).per_page(10).each do |user|
      assert_select 'a[href=?]', edit_user_path(user)
      assert_select 'a[href=?]', user_path(user)
    end
    
    get edit_user_path(User.paginate(page: 1)[4])
    assert_template 'users/edit'
    
    get users_path
    
    assert_difference 'User.count', -1 do
      delete user_path(User.paginate(page: 1)[5])
    end
    
  end
  
  test "index user as non-admin does not show edit and delete" do
    log_in_as(@non_admin_user)
    get users_path
    User.paginate(page: 1).per_page(10).each do |user|
      assert_select 'a[href=?]', edit_user_path(user), count: 0
      assert_select 'a[href=?]', user_path(user), count: 0
    end
  end
  
  test "admin can grant admin rights" do
    log_in_as(@user)
    patch user_path(@non_admin_user), user: { admin: true }
    @non_admin_user.reload
    assert @non_admin_user.admin?
  end
  
  test "non-admin cannot grant admin rights" do
    log_in_as(@non_admin_user)
    patch user_path(@non_admin_user), user: { admin: true }
    @non_admin_user.reload
    assert_not @non_admin_user.admin?
  end
end
