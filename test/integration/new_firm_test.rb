require 'test_helper'

class NewFirmTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:archer)
  end
  
  test "creating a firm with invalid data" do
    log_in_as @user
    get new_user_firm_path user_id: @user
    assert_template 'firms/new'
    assert_no_difference 'Firm.count' do
      post user_firms_path user_id: @user, firm: { name: "",
                                                   legal: "", 
                                                   employees: "",
                                                   sales: ""     }
    end
    assert_template 'firms/new'

  end
  
  test "creating a firm with valid data" do
    log_in_as @user
    get new_user_firm_path user_id: @user
    assert_difference 'Firm.count', 1 do
      post_via_redirect user_firms_path user_id: @user, firm: { name: "New Firm",
                                                  legal: "New Firm Inc.",
                                                  employees: 10,
                                                  sales: 10000 }
    end
    assert_template 'firms/show'
    assert_select 'h1',  "New Firm"
    assert_not flash.empty?
  end
  
  
end
