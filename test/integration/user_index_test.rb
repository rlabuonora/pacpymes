require 'test_helper'

class UserIndexTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:rafa)
  end
  
  test "users index should show pagination" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    User.paginate(page: 1).per_page(5).each do |user|
      assert_select 'a[href=?]', user_path(user)
    end
  end
end
