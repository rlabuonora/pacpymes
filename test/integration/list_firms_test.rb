require 'test_helper'

class ListFirmsTest < ActionDispatch::IntegrationTest
 
  def setup
    @admin = users(:rafa)
    @non_admin = users(:archer)
  end
  
  test "listing firms as admin shows all firms" do
    log_in_as @admin
    get user_firms_path @admin
    Firm.paginate(page: 1).per_page(10).each do |firm|
      assert_select 'a[href=?]', user_firm_path(user_id: firm.user, id: firm)
      assert_match firm.asesor.name, response.body if firm.asesor
    end
  end
  
  test "listing firms as non-admin shows only non admin's firms" do
    log_in_as @non_admin
    get user_firms_path @non_admin
    @non_admin.firms.paginate(page: 1).per_page(10).each do |firm| 
      # link para editar la empresa
      assert_select 'a[href=?]', user_firm_path(user_id: @non_admin, id: firm)
      # TODO: hay una columa perteneciente al usuario
    end
    
    Firm.where.not(user_id: @non_admin).each do |firm|
      assert_no_match firm.name, response.body
    end
  end
 
end
