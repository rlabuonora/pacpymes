require 'test_helper'

class EditFirmTest < ActionDispatch::IntegrationTest
  
  def setup
    @firm = firms(:two)
    @admin = users(:rafa)
    @non_admin = users(:archer)
  end
  
  test "un admin puede asignar un asesor" do
    log_in_as(@admin)
    patch user_firm_path(id: @firm.id, user_id: @firm.user.id), firm: { name: "Mi empresa", asesor_id: @admin.id }
    @firm.reload
    assert_equal @admin, @firm.asesor
    assert_equal "Mi empresa", @firm.name

  end
  
  test "un no-admin puede editar los datos de su empresa" do
    log_in_as(@non_admin)
    firm = @non_admin.firms.first
    patch user_firm_path(id: firm.id, user_id: firm.user.id), firm: { name: "Mi empresa" }
    firm.reload
    assert_equal "Mi empresa", firm.name
  end
  
  
  
  test "un no-admin no puede editar el asesor de su empresa" do
    log_in_as(@non_admin)
    firm = @non_admin.firms.first
    firm.asesor = nil
    firm.save
    firm.reload
    assert_nil firm.asesor
    patch user_firm_path(id: firm.id, user_id: firm.user.id), firm: { asesor_id: @admin.id }
    firm.reload
    assert_nil firm.asesor
  end
end
