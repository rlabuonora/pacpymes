require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  
  test "full title helper" do
    assert_equal full_title, "PACPYMES"
    assert_equal full_title("¿Qué es?"), "¿Qué es? | PACPYMES"
  end
  
end
