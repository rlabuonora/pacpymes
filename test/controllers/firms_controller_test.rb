require 'test_helper'

class FirmsControllerTest < ActionController::TestCase
  
  def setup
    @admin = users(:rafa)
    @non_admin = users(:archer)
    @non_admin_wrong = users(:lana)
    
    @archers = firms(:one)
  end
  
  test "should redirect index, show, new, create, edit, update, destroy if not logged in" do
    get :index, user_id: @non_admin
    assert_redirected_to root_url
   
    get :show, user_id: @non_admin, id: @archers
    assert_redirected_to root_url
   
    get :new, user_id: @non_admin
    assert_redirected_to root_url
   
    post :create, user_id: @non_admin, id: @archers, firm: { name:     "Example Firm",
                                                             legal:    "Example Firm S.A.",
                                                             industry: "Commerce",
                                                             sales:     1000000             }
    assert_redirected_to root_url
   
    get :edit, user_id: @non_admin, id: @archers
    assert_redirected_to root_url
   
    get :update, user_id: @non_admin, id: @archers,  firm: { name:     "New Example Firma",
                                                             legal:    "New Example Firm S.A.",
                                                             industry: "Toys!",
                                                             sales:     1000000             }
    assert_redirected_to root_url
  end
  
  test "should redirect index, show, new, create, edit, update, destroy if logged in as wrong user" do
    
    log_in_as @non_admin
    get :index, user_id: @non_admin_wrong
    assert_redirected_to root_url
    
    get :show, user_id: @non_admin_wrong, id: @non_admin_wrong.firms.first
    assert_redirected_to root_url
    
    get :new, user_id: @non_admin_wrong
    assert_redirected_to root_url
    
   
    post :create, user_id: @non_admin_wrong, firm: { name: "Example Firm", 
                                                     legal: "Example Inc.",
                                                     industry: "Example Industry",
                                                     sales: 200000,
                                                     employees: 90 }
    assert_redirected_to root_url
    
    get :edit, user_id: @non_admin_wrong, id: @non_admin_wrong.firms.first
    assert_redirected_to root_url
    
    patch :create, user_id: @non_admin_wrong, id: @non_admin_wrong.firms.first, 
                                              firm: { name:         "Example Firm", 
                                                      legal:        "Example Inc.",
                                                      industry:     "Example Industry",
                                                      sales:         200000,
                                                      employees:     90 }
    assert_redirected_to root_url
   
  end

  test "should get show when logged in as admin" do
    log_in_as @admin
    get :show, user_id: @non_admin, id: @non_admin.firms.first
    assert_response :success
  end

  test "should get edit when logged in as admin" do
    log_in_as @admin
    get :edit, user_id: @non_admin, id: @non_admin.firms.first
    assert_response :success
  end



  
  # Test for correct user
  
  test "should get show when logged in as correct user" do
    log_in_as @non_admin
    get :show, user_id: @non_admin, id: @non_admin.firms.first
    assert_response :success
  end

  test "should get edit when logged in as correct user" do
    log_in_as @non_admin
    get :edit, user_id: @non_admin, id: @non_admin.firms.first
    assert_response :success
  end
end
