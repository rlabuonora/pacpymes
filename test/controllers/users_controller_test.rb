require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:rafa)
    @another_user = users(:archer)
    @non_admin_user = users(:lana)
  end
  
  test "should get new" do
    get :new
    assert_response :success
    assert_select "title", "Registrarse | PACPYMES"
  end
  
  test "should not get edit when not logged in" do
    get :edit, id: @user
    assert_redirected_to root_url
    assert_not flash.empty?
  end
  
  test "should not post to update when not logged in" do
    patch :update, id: @user, user: { name:  "Juancho",
                                      email: " juancho@gmail.com" }
    assert_redirected_to root_url
    assert_not flash.empty?              
  end
  
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@another_user)
    get :edit, id: @user
    assert_redirected_to root_url
    assert flash.empty?
  end
  
  test "should get edit when logged in as admin" do
    log_in_as(@user)
    get :edit, id: @another_user
    assert_template 'users/edit'
  end
  
  test "should update when logged in as admin" do
    log_in_as(@user)
    patch :update, id: @another_user, user: { name: "Ricardo Iorio",
                                              email: "riorio@gmail.com" }
    assert_redirected_to user_path(@another_user)
    assert_not flash.empty?
  end
  
  test "should redirect index when not admin" do
    log_in_as(@non_admin_user)
    get :index
    assert_redirected_to root_url
  end
  
  
  test "should redirect destroy when not admin" do
    log_in_as(@non_admin_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @another_user
    end
    assert_redirected_to root_url
  end
  
  test "should destroy when deleted by admin" do
    log_in_as(@user)
    assert_difference 'User.count', -1 do
      delete :destroy, id: @another_user
    end
    assert_not flash.empty?
  end
  
  test "should redirect update when logged in as wrong user" do
    log_in_as(@another_user)
    patch :update, id: @user, user: { name:  "Juancho",
                                      email: "juancho@gmail.com" }
    assert_redirected_to root_url
    assert flash.empty?
  end
  
  
  test "should not allow to delete self" do
    log_in_as @user
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
    
  end
end
