require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  def setup
    @base_title = 'PACPYMES'
  end
  
  test "should get home" do
    get :home
    assert_response :success
    assert_select 'title', full_title
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select 'title', full_title("Contacto")
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select 'title', full_title("¿Qué es?")
  end
  
  test "should get how" do
    get :how
    assert_response :success
    assert_select 'title', full_title("¿Cómo funciona?")
  end

end
