class CreateFirms < ActiveRecord::Migration
  def change
    create_table :firms do |t|
      t.string :name
      t.string :legal
      t.string :industry
      t.integer :sales
      t.integer :employees
      t.string :state
      t.references :user, index: true
      t.timestamps null: false
    end
  end
end
