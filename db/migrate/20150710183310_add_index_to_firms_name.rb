class AddIndexToFirmsName < ActiveRecord::Migration
  def change
    add_index :firms, :name, unique: true
  end
end
