class AddAsesorToFirm < ActiveRecord::Migration
  def change
    add_reference :firms, :asesor, index: true, foreign_key: true
  end
end
