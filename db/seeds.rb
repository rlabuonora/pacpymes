User.destroy_all
User.create!(name:                  "Rafael La Buonora",
             email:                 "rlabuonora@yahoo.com",
             password:              "password",
             password_confirmation: "password",
             admin:                  true)
         
User.create!(name:                  "Steve Jobs",
             email:                 "steve@apple.com",
             password:              "password",
             password_confirmation: "password",
             admin:                  false)    
             
30.times do |n|
  name = Faker::Name.name
  email = Faker::Internet.email
  password = "password"
  User.create!(name: name, 
               email: email, 
               password: password, 
               password_confirmation: password)
end


Firm.destroy_all

20.times do
  name = Faker::Company.name
  legal = "#{name} #{Faker::Company.suffix}"
  industry = "Commerce"
  sales = 1000000
  employees = 100
  user = User.all[Random.rand(User.count)]
  Firm.create!(name: name, legal: legal, industry: industry, sales: sales, employees: employees, user:user)
end