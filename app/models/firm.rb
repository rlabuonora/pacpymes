class Firm < ActiveRecord::Base
  belongs_to :user
  belongs_to :asesor, class_name: 'User'
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :legal, presence: true, uniqueness: { case_sensitive: false }
  validates :employees, presence: true
  validates :sales, presence: true
  validate :asesor_must_be_admin
  
  def asesor_must_be_admin
    errors.add(:asesor, "Asesor tiene que ser admin") if asesor && !asesor.admin
  end
end
