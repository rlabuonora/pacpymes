class User < ActiveRecord::Base
  
  
  scope :admins, ->  { where(admin: true) }
  before_save { self.email = email.downcase }
  has_many :firms
  has_many :asesoradas, class_name: "Firm", foreign_key: "asesor_id"
  
  EMAIL_REGEXP = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  validates :name,  presence: { message: "no puede estar vacío" }, length: { maximum: 50  }
  validates :email, presence: { message: "no puede estar vacío" }, length: { maximum: 255 },
                    format:     { with: EMAIL_REGEXP },
                    uniqueness: { case_sensitive: false, message: "ya está en uso." }
  validates :password, presence: { message: "no puede estar vacío" }, 
                       length:   { minimum: 6, message: "debe tener 6 caracteres o más"},
                       allow_nil: true
        
  has_secure_password      
  
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

end
