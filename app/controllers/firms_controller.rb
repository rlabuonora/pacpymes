class FirmsController < ApplicationController
  
  before_action :require_login
  before_action :require_correct_user_or_admin

  def index
    if current_user.admin?
      @firms  = Firm.all.paginate(page: params[:page_id])
    else 
      @firms = current_user.firms.paginate(page: params[:page_id])
    end
  end

  def show
    @firm = Firm.find(params[:id])
  end

  def new
    @user = current_user
    @firm = @user.firms.new
  end

  def create
    @firm = current_user.firms.build(firm_params)
    if @firm.save
      # handle success
      flash[:success] = "Empresa registrada"
      redirect_to user_firm_path(id: @firm.id, user_id: @user.id)
    else
      render 'new'
    end
  end

  def edit
    @firm = Firm.find(params[:id])
  end

  def update
    @firm = Firm.find(params[:id])
    if !current_user.admin? && firm_params.include?(:asesor_id)
      redirect_to root_path
    elsif @firm.update_attributes(firm_params)
      flash[:success] = "Información Actualizada"
      redirect_to user_firm_path(user_id: current_user, id: @firm.id)
    else
      render 'edit'
    end
  end

  def destroy
    @firm = Firm.find(params[:id])
    @firm.destroy
    flash[:success] = "Empresa Eliminada"
    redirect_to root_url
  end
  
  private 
  
    def firm_params
      params.require(:firm).permit(:name, :legal, :employees, :sales, :industry, :asesor_id)
    end
  

end
