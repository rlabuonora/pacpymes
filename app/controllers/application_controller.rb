class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  include SessionsHelper
  private
  
    def require_login
      unless logged_in?
        store_location
        flash[:danger] = "Por favor inicie sesión"
        redirect_to root_url
      end
    end
    
    def require_correct_user
      @user = User.find(params[:user_id] || params[:id])
      redirect_to root_url unless current_user?(@user)
    end
    
    def require_admin
      redirect_to root_url unless current_user.admin?
    end
    
    def require_correct_user_or_admin
      @user = User.find(params[:user_id] || params[:id])
      redirect_to root_url unless current_user.admin? || current_user?(@user)
    end
    
end
