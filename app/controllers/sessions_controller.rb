class SessionsController < ApplicationController
  
  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_back_or user
    else
      flash[:danger] = "Correo/Contraseña inválidos"
      redirect_to root_path
    end
  end
  
  def destroy
    log_out
    redirect_to root_path
  end
end
