class StaticPagesController < ApplicationController
  def home
    redirect_to current_user if current_user
  end

  def contact
  end

  def about
  end
  
  def how
  end
end
