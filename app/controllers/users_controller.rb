class UsersController < ApplicationController
  
  before_action :require_login, only: [:index, :show, :edit, :update]
  before_action :require_admin, only: [:index, :destroy]
  before_action :require_correct_user_or_admin,  only: [:edit, :update]
  
  def index
    @users = User.paginate(page: params[:page]).per_page(10)
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = 'Bienvenido a PACPYMES, por favor registre a su empresa.'
      redirect_to @user
    else
      render 'new'
    end
  end
  
  
  def edit 
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # handle success
      flash[:success] = "Información Actualizada"
      redirect_to @user
    else
      #handle failure
      render 'edit'
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    if @user != current_user
      @user.destroy
      flash[:success] = "Usuario Eliminado"
      redirect_to users_url
    else
      redirect_to root_url
    end
  end
  
  
  private
  
    def user_params
      if current_user && current_user.admin?
        params.require(:user).permit(:name, :email, :password, :password_confirmation, :admin)
      else
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      end
    end
    
    
end
