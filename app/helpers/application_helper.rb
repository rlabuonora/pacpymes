module ApplicationHelper
  def full_title(title='')
    base_title = "PACPYMES"
    if title.blank?
      base_title
    else
      "#{title} | #{base_title}"
    end
  end
  
  
  def detalles_icon
    content_tag(:span, "", class: "glyphicon glyphicon-zoom-in", title: "Detalles")
  end
  
  def editar_icon
    content_tag(:span, "", class: "glyphicon glyphicon-pencil", title: "Editar")
  end
  
  def admin_icon(user)
    if user.admin?
      content_tag :span, "", class: "glyphicon glyphicon-ok"
    else
      content_tag :span, "", class: "glyphicon glyphicon-remove"
    end
  end
  
  def borrar_icon
    content_tag(:span, "", class: "glyphicon glyphicon-trash", title: "Eliminar")
  end
end
